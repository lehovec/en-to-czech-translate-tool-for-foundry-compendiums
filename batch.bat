node main.js ./node_modules/dnd5e/packs/classes.db ./vocabularies/classes.json ./out/compendium-dnd5e-cz-classes.db
node main.js ./node_modules/dnd5e/packs/classfeatures.db ./vocabularies/feats.json ./out/compendium-dnd5e-cz-classes-feats.db
node main.js ./node_modules/dnd5e/packs/items.db ./vocabularies/items.json ./out/compendium-dnd5e-cz-items.db
node main.js ./node_modules/dnd5e/packs/monsterfeatures.db ./vocabularies/monsterFeatures.json ./out/compendium-dnd5e-cz-monster-features.db
node main.js ./node_modules/dnd5e/packs/monsters.db ./vocabularies/monsters.json ./out/compendium-dnd5e-cz-monsters.db
node main.js ./node_modules/dnd5e/packs/races.db ./vocabularies/race.json ./out/compendium-dnd5e-cz-race-feats.db
node main.js ./node_modules/dnd5e/packs/spells.db ./vocabularies/spells.json ./out/compendium-dnd5e-cz-spells.db
node main.js ./node_modules/dnd5e/packs/tradegoods.db ./vocabularies/goods.json ./out/compendium-dnd5e-cz-goods.db


