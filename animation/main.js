const fs = require('fs');
const readline = require('readline');
const stream = require('stream');
const {mergeDeepRight, whereEq, map} = require('ramda')

var myArgs = process.argv.slice(2);

let rawdata = fs.readFileSync(myArgs[1]);
let translation = JSON.parse(rawdata);

const instream = fs.readFileSync(myArgs[0]);
let source = JSON.parse(instream);

source = map((item) => {
	for (var i of translation) {
		if (i.original.toLowerCase() === item.toLowerCase()) {
			return i.data.name;
		}
	}
	return item;
}, source)

fs.writeFile(myArgs[2], JSON.stringify(source), () => {})