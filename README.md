Tool pro překlad Foundry VTT 5e kompendia dle definovaných slovníků.

## Instalace

Knihovny se stáhnou pomocí npm nebo yarn

`npm install`

nebo

`yarn install`

## Použití

`node main.js [zdrojový .db soubor] [json slovnik] [výstup]`

Lze také použít `batch.bat` soubor pro hromadnou translaci compendií

Protože se kompendia tahají z gitu, je potřeba referencovat na ně do `node_modules`:

např. `node main.js ./node_modules/dnd5e/packs/classes.db ./vocabularies/classes.json ./out/compendium-dnd5e-cz-classes.db`

## Podoba slovníků

```javascript
[
  {
    "original": "Lair Actions", // jméno podle kterého se položka přeloží (libovolně hluboko ve struktuře)
    "condition": { // (optional) dodatečná podmínka pro aplikování překladu (např, položka musí mít jméno Aboleth)
      "name": "Aboleth"
    },
    "data": { // Aplikovaná data, nahradí dané klíče v json stromu novými hodnotami, můžou být libovolné, aplikují se na daný úsek stromu
      "name": "Akce doupěte",
      "data.description.value": "<p>Když abolet bojuje uvnitř svého doupěte, může využít okolní magii, aby provedla akce doupěte. Na iniciativu 20 (prohrávající všechny remízy) může abolet provést akci doupěte, která způsobí jeden z následujících účinků:</p><ul><li>Abolet sešle <em>přelud</em> (bez složek) na libovolný počet tvorů, které vidí do 12 sáhů od sebe. Dokud se abolet soustředí na tento účinek, nemůže provádět jiné akce doupěte. Uspěje-li cíl v záchranném hodu, nebo skončí-li pro něj účinek, bude imunní vůči aboletově akci doupěte <em>přelud</em> 24 hodin, i když takový tvor si může zvolit, že bude ovlivněn.</li> <li>Vodní tůň do 18 sáhů od aboleta se vyvalí mohutným proudem. Každý tvor na zemi do 4 sáhů od takové tůně musí uspět v záchranném hodu na Sílu se SO 14, jinak je přitažen až 4 sáhy do vody a sražen k zemi. Abolet nemůže použít tuto akci doupěte znovu, dokud nepoužije jinou.</li> <li>Voda v aboletově doupěti se magicky stane prostředníkem jeho hněvu. Abolet může zacílit libovolný počet tvorů, které vidí v takové vodě do 18 sáhů od sebe. Cíl musí uspět v záchranném hodu na Moudrost se SO 14, jinak utrpí psychické zranění 7 (2k6). Abolet nemůže použít tuto akci doupěte znovu, dokud nepoužije jinou.</li></ul>"
    }
  }, 
  ...
]
```

Pozor, při každém generování se vygeneruje nové ID záznamu (takže referencování se s novou verzí rozbije, tohle musím opravit)

__Bonus, ve složce animation je scriptík pro překlad slovníků pro modul Automate Animations, díky tomu se částečně budou provádět animace pro kouzla a schopnosti s českými názvy__