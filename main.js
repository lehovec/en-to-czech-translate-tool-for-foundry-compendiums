const fs = require('fs');
const readline = require('readline');
const stream = require('stream');
const {mergeDeepRight, whereEq} = require('ramda')

var myArgs = process.argv.slice(2);

let rawdata = fs.readFileSync(myArgs[1]);
let czMonsters = JSON.parse(rawdata);

const instream = fs.createReadStream(myArgs[0]);
const outstream = new stream;
const rl = readline.createInterface(instream, outstream);

const unflatten = function unflatten(data) {
	"use strict";
	if (Object(data) !== data || Array.isArray(data))
		return data;
	var regex = /\.?([^.\[\]]+)|\[(\d+)\]/g,
		resultholder = {};
	for (var p in data) {
		var cur = resultholder,
			prop = "",
			m;
		while (m = regex.exec(p)) {
			cur = cur[prop] || (cur[prop] = (m[2] ? [] : {}));
			prop = m[2] || m[1];
		}
		cur[prop] = data[p];
	}
	return resultholder[""] || resultholder;
};

function getObject(theObject, fullObject) {
	if (theObject instanceof Array) {
		for (let i = 0; i < theObject.length; i++) {
			theObject[i] = getObject(theObject[i], fullObject);
		}
	} else {
		for (let prop in theObject) {
			if (theObject[prop] instanceof Object || theObject[prop] instanceof Array) {
				theObject[prop] = getObject(theObject[prop], fullObject);
			}
		}
		if (typeof theObject['_id'] !== 'undefined') {
			theObject = replaceLang(theObject, fullObject)
		}
	}
	return theObject;
}


function replaceLang(object, fullObject) {
	for (let translate of czMonsters) {
		if (typeof object.name !== 'undefined' && translate.original.toLowerCase().trim() == object.name.toLowerCase().trim() &&
			(typeof translate.condition === 'undefined' || whereEq(translate.condition, fullObject))) {
			object._id = (Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)).substr(0, 16);
			return mergeDeepRight(object, unflatten(translate.data))
		}
	}
	if (typeof object.name !== 'undefined') {
		console.log(`Object ${object.name} [${object._id}] wasn't translated`)
	}
	return object
}

fs.truncate(myArgs[2], 0, function () {
})
var newFile = fs.createWriteStream(myArgs[2], {
	flags: 'a' // 'a' means appending (old data will be preserved)
})

rl.on('line', function (line) {
	const currentMonster = JSON.parse(line);

	const newObject = getObject(currentMonster, currentMonster)

	newFile.write(JSON.stringify(newObject) + '\n')

});

rl.on('close', function () {
	newFile.end()
});
